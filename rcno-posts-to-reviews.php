<?php
/**
 * Plugin Name:     Recencio Post to Reviews
 * Plugin URI:      https://wzymedia.com
 * Description:     An example extension for converting WordPress posts to reviews for use with the Recencio Book Review plugin
 * Author:          w33zy
 * Author URI:      https://wzymedia.com
 * Text Domain:     wzy-media
 * Domain Path:     /languages
 * Version:         1.0.0
 *
 * @package         wzy_Media
 */

if ( ! defined( 'ABSPATH' ) ) {
	return;
}


register_activation_hook( __FILE__, function() {

	include_once ABSPATH . 'wp-admin/includes/plugin.php';

	if ( ! is_plugin_active( 'recencio-book-reviews/recencio-book-reviews.php' ) ) {
		wp_die( 'Please make sure the Recencio Book Review plugin is installed and activated.', 'wzy-media' );
	}
	
} );


if ( ! class_exists( 'Rcno_Posts_To_Reviews' ) ) {

	require_once WP_PLUGIN_DIR . '/recencio-book-reviews/includes/abstracts/Abstract_Rcno_Extension.php';

	/**
	 * Class Rcno_Posts_To_Reviews
	 */
	class Rcno_Posts_To_Reviews extends Abstract_Rcno_Extension {

		/**
		 * Rcno_Custom_User_Metadata constructor.
		 */
		public function __construct() {
			$this->id       = 'rcno_posts_to_reviews';
			$this->image    = plugin_dir_url( __FILE__ ) . 'assets/images/rcno-posts-to-reviews.png';
			$this->title    = __( 'Convert Posts to Reviews', 'rcno-reviews' );
			$this->desc     = __( 'Converts WordPress posts to Recencio book reviews and Recencio book reviews to WordPress posts.', 'rcno-reviews' );
			$this->settings = false;
		}

		/**
		 * All methods that we want to be called by the Rcno_Reviews_Extensions class goes here.
		 */
		public function load() {
			$this->add_filters();
			$this->add_actions();
			return true;
		}

		/**
		 * Add WordPress filters are called here.
		 */
		private function add_filters() {
			add_filter( 'post_row_actions', array( $this, 'add_link' ), 10, 2 );
		}

		/**
		 * Add WordPress actions are called here.
		 */
		private function add_actions() {
			add_action( 'admin_init', array( $this, 'register_settings' ) );
			add_action( 'rcno_extensions_settings_page_footer', array( $this, 'render_settings_page' ) );
			add_action( 'wp_ajax_convert_post_review', array( $this, 'convert_post_review' ) );
			add_action( 'admin_print_footer_scripts', array( $this, 'admin_script' ) );
		}

		/**
		 * Adds a link to the WP admin posts table
		 *
		 * @since 1.0.0
		 *
		 * @param array $actions An array of row action links
		 * @param WP_Post $post  The post object
		 *
		 * @return array
		 */
		public function add_link( $actions, $post ) {

			$post_id = $post->ID;
			$nonce   = wp_create_nonce( 'convert-to-review' );

			// Check for the default type.
			// You can check if the current user has some custom rights.
			if ( ( 'post' === $post->post_type ) && current_user_can( 'edit_post', $post->ID ) ) {

				// Add the new 'Convert' quick link.
				$actions = array_merge( $actions, array(
					'convert' => sprintf( '<a href="#" class="convert-to-review" data-post-id="%1$s" data-nonce="%4$s" data-post-type="%5$s" title="%3$s">%2$s</a>',
						$post_id,
						__( 'Convert', 'rcno-reviews' ),
						__( 'Convert to Review', 'rcno-reviews' ),
						$nonce,
						$post->post_type
					)
				) );
			}

			if ( ( 'rcno_review' === $post->post_type ) && current_user_can( 'edit_post', $post->ID ) ) {

				// Add the new 'Convert' quick link.
				$actions = array_merge( $actions, array(
					'convert' => sprintf( '<a href="#" class="convert-to-post" data-post-id="%1$s" data-nonce="%4$s" data-post-type="%5$s" title="%3$s">%2$s</a>',
						$post_id,
						__( 'Convert', 'rcno-reviews' ),
						__( 'Convert to Post', 'rcno-reviews' ),
						$nonce,
						$post->post_type
					)
				) );
			}

			return $actions;
		}

		/**
		 * Handles the AJAX request
		 *
		 * @since 1.0.0
		 *
		 * @uses check_admin_referer()
		 * @uses set_post_type()
		 * @uses wp_send_json_success()
		 * @uses wp_send_json_error()
		 *
		 * @return void
		 */
		public function convert_post_review() {

			check_admin_referer( 'convert-to-review', 'nonce' );

			$post_id   = ! empty( $_POST['postID'] ) ? (int) $_POST['postID'] : 0;
			$post_type = ! empty( $_POST['postType'] ) ? sanitize_text_field( $_POST['postType'] ) : null;

			if ( $post_id && 'post' === $post_type ) {
				$result = set_post_type( $post_id, 'rcno_review' );

				if ( $result ) {
					wp_send_json_success();
				}
			}

			if ( $post_id && 'rcno_review' === $post_type ) {
				$result = set_post_type( $post_id, 'post' );

				if ( $result ) {
					wp_send_json_success();
				}
			}

			wp_send_json_error();
		}

		/**
		 * Adds our script on the WP admin pages for posts and rcno_reviews
		 *
		 * @since 1.0.0
		 *
		 * @uses get_current_screen()
		 *
		 * @return void
		 */
		public function admin_script() {

			$screen = get_current_screen();

			if ( $screen && ( 'edit-rcno_review' === $screen->id || 'edit-post' === $screen->id ) ) { ?>
				<script>
                  window.jQuery(document).ready(function($) {

                    $('.convert-to-review, .convert-to-post').on('click', function(e) {
                      e.preventDefault();

                      var postID = $(this).data('post-id');
                      var postType = $(this).data('post-type');
                      var nonce = $(this).data('nonce');

                      $.ajax({
                        method: 'POST',
                        url: window.ajaxurl,
                        data: {
                          action: 'convert_post_review',
                          nonce,
                          postID,
                          postType,
                        },
                      }).done(function(res) {
                        if (res.success === true) {
                          $('tr#post-' + postID).hide();
                        }
                      }).fail(function(res) {
                        console.log(res);
                      });

                    });

                  });
				</script>
			<?php }
		}

		/**
		 * Registers the settings to be stored to the WP Options table.
		 */
		public function register_settings() {
			register_setting( 'rcno-posts-to-reviews', 'rcno_posts_to_reviews_options', array(
				'sanitize_callback' => array( $this, 'sanitize_settings' )
			) );
		}

		/**
		 * The hidden markup the is rendered by the Thickbox modal window.
		 */
		public function render_settings_page() {
			include __DIR__ . '/includes/settings-page.php';
		}


		/**
		 * Looks to see if the specified setting exists, returns default if not.
		 *
		 * @param string $key
		 * @param mixed  $default
		 *
		 * @return mixed
		 */
		protected function get_setting( $key, $default = '' ) {

			if ( empty( $key ) ) {
				return $default;
			}

			$settings = get_option( 'rcno_posts_to_reviews_options', array() );
			return ! empty( $settings[ $key ] ) ? $settings[ $key ] : $default;
		}

		/**
		 * Sanitize the settings being saved by this extension.
		 *
		 * @param array $settings The settings array for the extension.
		 *
		 * @return array
		 */
		public function sanitize_settings( array $settings ) {
			foreach ( $settings as $key => $value ) {
				$settings[ $key ] = sanitize_text_field( $value );
			}
			return $settings;
		}

	}

}

/**
 * Register the extension with the main Recencio plugin by adding its info
 * via the 'rcno_reviews_extensions' filter.
 *
 * This is how we tell the Recencio plugin about our extension
 * We are using a anonymous function here to avoid function name clashes with other extensions.
 *
 * @param  array $extensions
 *
 * @return array
 */
add_filter( 'rcno_reviews_extensions', function ( $extensions ) {
	$extensions['rcno_posts_to_reviews'] = 'Rcno_Posts_To_Reviews';
	return $extensions;
} );
